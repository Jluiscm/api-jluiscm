console.log("Aqui funcionando con nodemon");

var movimientosJSON = require('./movimientosv2.json');
var usuariosJSON = require('./usuariosv1.json');
var express = require('express');
var bodyParser = require('body-parser');
var requestJson = require ('request-json');

var app = express();
app.use(bodyParser.json())

app.get('/',function(req,res) {
    res.send('Hola mundo-API');
});

app.get('/v1/movimientos',function(req,res) {
    //sendfile is deprecated
    res.sendfile('movimientosv1.json');
});

app.get('/v2/movimientos',function(req,res) {
    //sendfile is deprecated
    //res.sendfile('movimientosv2.json');
    res.send(movimientosJSON);
});

app.get('/v2/movimientosq',function(req,res) {
    console.log(req.query);
    res.send('Recibido');
});

app.get('/v2/movimientos/:id',function(req,res) {
    res.send(movimientosJSON[req.params.id-1]);
});

app.post('/v2/movimientos',function(req,res) {
    console.log(req);
    if(req.headers['authorization']!=undefined){
      var nuevo=req.body;
      nuevo.id = movimientosJSON.length+1;
      movimientosJSON.push(nuevo);
      res.send('Requerimiento dado de alta');
    }else{
      res.send('No esta autorizado');
    }
});

app.put('/v2/movimientos/:id',function(req,res) {
    var actual=movimientosJSON[req.params.id-1];
    var modificado=req.body;
    if(modificado.importe != undefined){
      actual.importe=modificado.importe;
    }
    if(modificado.ciudad != undefined){
      actual.ciudad=modificado.ciudad;
    }
    res.send('Requerimiento Modificado');
});

app.delete('/v2/movimientos/:id',function(req,res){
    var actual=movimientosJSON[req.params.id-1];
    movimientosJSON.push({
      "id": movimientosJSON.length+1,
      "ciudad": actual.ciudad,
      "importe": actual.importe * (-1),
      "concepto": "negativo del " + req.params.id
    });
    res.send('Movimiento anulado');
});

app.get('/v2/usuarios',function(req,res) {
    res.send(usuariosJSON);
});

app.get('/v2/usuarios/:id',function(req,res) {
    res.send(usuariosJSON[req.params.id-1]);
});

app.post('/v2/usuarios/login',function(req,res) {
    console.log(req.headers);
    //var consulta = jsonQuery('[email='+req.headers.usuario+']',{data:usuariosJSON})
    for(i=0;i<usuariosJSON.length;i++){
      var usr = usuariosJSON[i];
      if(usr.email==req.headers.usuario && usr.pass==req.headers.pass){
        usr.estado="Autenticado";
        res.send('Usuario Autenticado');
        break;
      }
    }
    res.send('Usuario/Pass incorrecto');
});

app.post('/v2/usuarios/logout',function(req,res) {
    console.log(req.headers);
    for(i=0;i<usuariosJSON.length;i++){
      var usr = usuariosJSON[i];
      if(usr.id==req.headers.idusr){
        usr.estado="null";
        res.send('Usuario Desconectado');
        break;
      }
    }
    res.send('El usuario no estaba conectado');
});

//version 3 de la API conectada a mlab
var urlMlabRaiz="https://api.mlab.com/api/1/databases/techujlcmmx/collections";
var apiKey = "apiKey=XdS26VYeiXpzps4RVXXqsT2MMQrCiMV3";
var clienteMlab=requestJson.createClient(urlMlabRaiz+"?"+apiKey);

app.get('/v3', function(req, res){
  clienteMlab.get('', function(err, resM, body){
    var coleccionesUsuario = [];
    if (!err) {
      for (var i = 0; i < body.length; i++){
        if(body[i] != "system.indexes"){
          coleccionesUsuario.push({"recurso":body[i],"url":"/v3/"+body[i]})
        }
      }
      res.send(coleccionesUsuario);
    }else {
      res.send(err);
    }

  });
});

app.get('/v3/usuarios',function(req,res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz+"/usuarios?"+apiKey);
    clienteMlab.get('', function(err, resM, body){
      res.send(body);
    });
});

app.get('/v3/usuarios/:id',function(req,res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz+"/usuarios");
    clienteMlab.get('?q={"idusuario":"'+req.params.id+'"}&'+apiKey, function(err, resM, body){
      res.send(body);
    });
});

app.post('/v3/usuarios',function(req,res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz+"/usuarios?"+apiKey);
    clienteMlab.post('',req.body, function(err, resM, body){
      res.send(body);
    });
});

app.put('/v3/usuarios/:id',function(req,res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz+"/usuarios");
    var cambio= '{"$set":'+JSON.stringify(req.body)+'}';
    //res.send('?q={"$set":'+req.body+'}&'+apiKey);
    clienteMlab.put('?q={"id":'+req.params.id+'}&'+apiKey,JSON.parse(cambio), function(err, resM, body){
      res.send(body);
    });
});

app.get('/v3/movimientos',function(req,res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz+"/movimientos?"+apiKey);
    clienteMlab.get('', function(err, resM, body){
      res.send(body);
    });
});

app.get('/v3/movimientos/:idcuenta',function(req,res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz+"/movimientos");
    clienteMlab.get('?'+apiKey+'&q={"idcuenta":"'+req.params.idcuenta+'"}', function(err, resM, body){
      res.send(body);
    });
});

//version 4 de la API
app.get('/v4',function(req,res) {
    //sendfile is deprecated
    res.sendfile('dummy.pdf');
});


/*
var urlMlabRaiz="https://api.mlab.com/api/1/databases/techujlcmmx/collections";
var apiKey = "apiKey=XdS26VYeiXpzps4RVXXqsT2MMQrCiMV3";
var clienteMlab=requestJson.createClient(urlMlabRaiz+"?"+apiKey);
*/
//Versiòn 5: Manejo de cuentas
app.get('/v5/cuentas/:usuario',function(req,res) {
    console.log(urlMlabRaiz+"/cuentas"+'?'+apiKey+'&q={"usuario":"'+req.params.usuario+'"}');
    clienteMlab = requestJson.createClient(urlMlabRaiz+"/cuentas");
    clienteMlab.get('?'+apiKey+'&q={"usuario":"'+req.params.usuario+'"}&f={"idcuenta":1}', function(err, resM, body){
      res.send(body);
    });
});

app.get('/v5/cuentas',function(req,res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz+"/cuentas?"+apiKey);
    clienteMlab.get('', function(err, resM, body){
      res.send(body);
    });
});

app.get('/v5/movimientos/:idcuenta',function(req,res) {
    console.log(urlMlabRaiz+"/cuentas"+'?'+apiKey+'&q={"idcuenta":'+req.params.idcuenta+'}&f={"movimientos":1}&s={"fecha":1}');
    clienteMlab = requestJson.createClient(urlMlabRaiz+"/cuentas");
    clienteMlab.get('?'+apiKey+'&q={"idcuenta":'+req.params.idcuenta+'}&f={"movimientos":1}&s={"fecha":1}', function(err, resM, body){
      res.send(body);
    });
});

app.post('/v5/usuarios',function(req,res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz+"/usuarios?"+apiKey);
    clienteMlab.post('',req.body, function(err, resM, body){
      res.send(body);
    });
});

app.listen(3000);
console.log("Escuchando en el puerto 3000");
