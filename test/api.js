var mocha = require('mocha');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = chai.should();
var server = require('../server')

chai.use(chaiHttp); //configurar chai con el modulo http

//declarar conjunto de pruebas (test-suites)
//utilizar el modo breve
describe('Tests de conectividad', () => {
  it('Google funciona', (done) => {
    chai.request('http://www.google.com.mx').get('/').end((err, res) => {
        //console.log(res);
        res.should.have.status(200);
        done();
      })
  })
});

describe('Tests de api-jluis', () => {
  it('Raiz de la API contesta', (done) => {
    chai.request('http://localhost:3000').get('/v3').end((err, res) => {
        //console.log(res);
        res.should.have.status(200);
        done();
      })
  })
  it('Raiz de la API funciona', (done) => {
    chai.request('http://localhost:3000').get('/v3').end((err, res) => {
        //console.log(res);
        res.should.have.status(200);
        res.body.should.be.a('array');
        done();
      })
  })
  it('Raiz de la API devuelve 2 colecciones', (done) => {
    chai.request('http://localhost:3000').get('/v3').end((err, res) => {
        //console.log(res.body);
        res.should.have.status(200);
        res.body.should.be.a('array');
        res.body.length.should.be.eql(2);
        done();
      })
  })
  it('Raiz de la API devuelve los objetos correctos', (done) => {
    chai.request('http://localhost:3000').get('/v3').end((err, res) => {
        //console.log(res.body);
        res.should.have.status(200);
        res.body.should.be.a('array');
        res.body.length.should.be.eql(2);
        for (var i = 0; i < res.body.length; i++) {
          res.body[i].should.have.property('recurso');
          res.body[i].should.have.property('url');
        }
        done();
      })
  })
});

describe('Tests de api movimientos', () => {
  it('Raiz de la API movimientos contesta', (done) => {
    chai.request('http://localhost:3000').get('/v3/movimientos').end((err, res) => {
        res.should.have.status(200);
        done();
      })
  })
  it('Raiz de la API movimientos funciona', (done) => {
    chai.request('http://localhost:3000').get('/v3/movimientos').end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('array');
        done();
      })
  })
  it('Consulta movimiento concreto funciona', (done) => {
    chai.request('http://localhost:3000').get('/v3/movimientos/mx0001').end((err, res) => {
        console.log(res.body);
        res.should.have.status(200);
        res.body.should.be.a('array');
        res.body[0].should.have.property('idcuenta');
        done();
      })
  })
});
